/**
 * @brief Arquivo main do jogo da velha
 *
 * @date 30/05/15
 * @author Alexandre, Claudio, Everton, Tercio e Wallace
 * @version 1.0
 */
#include <iostream>
#include <time.h>
#include <graphics.h>

// Tipos -------------------------------------------------
typedef struct{
	void *img; /**< Imagem carregada */
	void *msk; /**< Mascara da imagem */
	float h; /**< Altura da imagem */
	float w; /**< Largadura da imagem */
}img_data_type;

typedef struct{
	int x;
	int y;
}position_type;

typedef struct{
	img_data_type image;
	position_type pos;
}object_data_type;


enum
{
	VAZIO,
	BOLA,
	X
};

typedef enum
{
	SELECT,
	PLAYER,
	CPU,
	END
}game_state_type;
// -------------------------------------------------------

// Declaracao das funcoes --------------------------------
void cpuIA();
void updateScreen();
bool verificaVelha();
void carregaImagens();
bool verificaVitoria(char play);
void printObject(object_data_type *obj);
void graphInitObject(img_data_type *objeto, const char* caminho, const char* caminhomsk);
// -------------------------------------------------------

// Variaveis ---------------------------------------------
object_data_type escurece;
object_data_type background;
object_data_type gota;
object_data_type x;

// frases
object_data_type player[2];
object_data_type cpu[2];
object_data_type player_venceu;
object_data_type cpu_venceu;
object_data_type escolha;
object_data_type jogar_novamente;
object_data_type deu_velha;

char player_play, cpu_play;
	
char campo[3][3] = {{VAZIO,VAZIO,VAZIO},{VAZIO,VAZIO,VAZIO},{VAZIO,VAZIO,VAZIO}};
int campo_pos[2][4] = {{120,340,540,740},{80,235,390,500}};
// -------------------------------------------------------

int main(int argc, char** argv)
{	
	bool cpu_drama = true;
	int i, j, mousex,mousey;
	game_state_type game_state = SELECT;

	// Inicializa a janela com a resolucao definida
	initwindow(800,600,"Jogo da Agua Velha");
	
	carregaImagens();
	
	setbkcolor(COLOR(234,255,240));
	
	for(;;) //while(true)
	{
		printObject(&background);

		for(i = 0; i < 3; ++i)
			for(j = 0; j < 3; ++j)
			{
				if(campo[i][j] == BOLA)
				{
					gota.pos.x = campo_pos[0][i] + 25;
					gota.pos.y = campo_pos[1][j];
					printObject(&gota);
				}
				else if(campo[i][j] == X)
				{
					x.pos.x = campo_pos[0][i];
					x.pos.y = campo_pos[1][j];
					printObject(&x);
				}
			}
		
		switch(game_state)
		{
			case SELECT:
				/*setcolor(COLOR(0,0,255));
				settextstyle(4,HORIZ_DIR,4);
				outtextxy(180,180,"Quer jogar com qual?");*/
				
				printObject(&escurece);
				
				escolha.pos.x = 150;
				escolha.pos.y = 180;
				printObject(&escolha);
				
				gota.pos.x = 500 - (gota.image.w/2);
				gota.pos.y = 300 - (gota.image.h/2);
				
				x.pos.x = 300 - (x.image.w/2);
				x.pos.y = 300 - (x.image.h/2);
				
				printObject(&gota);
				printObject(&x);

				if(ismouseclick(WM_LBUTTONDOWN))
				{
					getmouseclick(WM_LBUTTONDOWN,mousex,mousey);
					
					if((mousex > gota.pos.x) && (mousex < gota.pos.x+gota.image.w) && (mousey > gota.pos.y) && (mousey < gota.pos.y+gota.image.h))
					{
						player_play = BOLA;
						cpu_play = X;
						game_state = PLAYER;
					}
					
					if((mousex > x.pos.x) && (mousex < x.pos.x+x.image.w) && (mousey > x.pos.y) && (mousey < x.pos.y+x.image.h))
					{
						player_play = X;
						cpu_play = BOLA;
						game_state = PLAYER;
					}
					
				}
			break;
			
			case PLAYER:
				/*
				setcolor(COLOR(0,255,0));
				settextstyle(4,VERT_DIR,4);
				outtextxy(0,350,"PLAYER");
				
				setcolor(COLOR(255,0,0));
				settextstyle(4,VERT_DIR,4);
				outtextxy(750,300,"CPU");
				*/
				player[0].pos.x = 0;
				player[0].pos.y = 300 - player[0].image.h/2;
				printObject(&player[0]);
				
				cpu[1].pos.x = 740;
				cpu[1].pos.y = 300 - cpu[1].image.h/2;
				printObject(&cpu[1]);
				
				if(ismouseclick(WM_LBUTTONDOWN))
				{
					getmouseclick(WM_LBUTTONDOWN,mousex,mousey);
					
					for(i = 0; i < 3; ++i)
						if((mousex > campo_pos[0][i]) && (mousex < campo_pos[0][i+1]))
							for(j = 0; j < 3; ++j)
								if((mousey > campo_pos[1][j]) && (mousey < campo_pos[1][j+1]))
								{
									if(!campo[i][j])
										campo[i][j] = player_play;
									else
										break;
								
									if(verificaVitoria(player_play) || verificaVelha())
										game_state = END;
									else
										game_state = CPU;
								}
				}
			break;
			
			case CPU:
				/*setcolor(COLOR(255,0,0));
				settextstyle(4,VERT_DIR,4);
				outtextxy(0,350,"PLAYER");
				
				setcolor(COLOR(0,255,0));
				settextstyle(4,VERT_DIR,4);
				outtextxy(750,300,"CPU");*/
				player[1].pos.x = 0;
				player[1].pos.y = 300 - player[1].image.h/2;
				printObject(&player[1]);
				
				cpu[0].pos.x = 740;
				cpu[0].pos.y = 300 - cpu[0].image.h/2;
				printObject(&cpu[0]);
				
				if(cpu_drama)
				{
					cpu_drama = false;
					break;
				}
				
				delay(500);
				cpu_drama = true;

				cpuIA();
				
				if(verificaVitoria(cpu_play))
					game_state = END;
				else
					game_state = PLAYER;
			break;
			
			case END:
				char winner = VAZIO;
				
				gota.pos.x = 400 - (gota.image.w/2);
				gota.pos.y = 300 - (gota.image.h/2);
				
				x.pos.x = 400 - (gota.image.w/2);
				x.pos.y = 300 - (x.image.h/2);
				
				deu_velha.pos.x = 150;
				deu_velha.pos.y = 150;
				
				//setcolor(COLOR(0,0,255));
				//settextstyle(4,HORIZ_DIR,4);
				
				
				printObject(&escurece);
				
				if(verificaVitoria(cpu_play))
				{
					cpu_venceu.pos.x = 250;
					cpu_venceu.pos.y = 180;
					printObject(&cpu_venceu);
					//outtextxy(250,180,"CPU Venceu!");
					winner = cpu_play;
				}
				else if(verificaVitoria(player_play))
				{
					player_venceu.pos.x = 180;
					player_venceu.pos.y = 180;
					printObject(&player_venceu);
					//outtextxy(180,180,"Parabens, voce venceu!");
					winner = player_play;
				}

				if(winner == BOLA)
					printObject(&gota);
				else if(winner == X)
					printObject(&x);
				else
					printObject(&deu_velha);
					
				jogar_novamente.pos.x = 220;
				jogar_novamente.pos.y = 450;
				printObject(&jogar_novamente);
				
				if(ismouseclick(WM_LBUTTONDOWN))
				{
					getmouseclick(WM_LBUTTONDOWN,mousex,mousey);
					
					if((mousex > jogar_novamente.pos.x) && (mousex < jogar_novamente.pos.x+jogar_novamente.image.w) && (mousey > jogar_novamente.pos.y) && (mousey < jogar_novamente.pos.y+jogar_novamente.image.h))
					{
						cpu_play = VAZIO;
						player_play = VAZIO;
						game_state = SELECT;
						
						for(i = 0; i < 3; ++i)
							for(j = 0; j < 3; ++j)
								campo[i][j] = VAZIO;
					}					
				}
			break;
		}
		
		// Atualiza todos objetos em cena
		updateScreen();
		
		// D� o tempo entre execu��es, equivalente a 60fps
		delay((1/60.0) * 1000.0);
	}
	
	return 0;
}

// Atualiza a tela com o que foi desenhado
void updateScreen()
{
	int page = getactivepage();
	setvisualpage(page);
	
	page = page ? 0:1;
	setactivepage(page);
	cleardevice();
}

void printObject(object_data_type *obj)
{
	int mode = COPY_PUT;
	int x, y, img_y = obj->pos.y;

	if(obj->image.msk)
	{
	 	putimage(obj->pos.x, img_y, obj->image.msk, AND_PUT);
	 	mode = OR_PUT;
	 }
	
	putimage(obj->pos.x, img_y, obj->image.img, mode);
}

void graphInitObject(img_data_type *objeto, const char* caminho, const char* caminhomsk)
{
	unsigned size;
	int left, top, right, bottom;
	
	int page = getactivepage();
	setactivepage(2);
	
	cleardevice();
	left = 0;
	top = 0;
	right = objeto->w;
	bottom = objeto->h;
	
	readimagefile(caminho,left, top, right, bottom);
	size= imagesize(left, top,right,bottom);
	objeto->img = new int[size];
	getimage(left, top, right, bottom, objeto->img);

	cleardevice();
	
	if(strcmp(caminhomsk," ")){
		readimagefile(caminhomsk,left, top, right, bottom );
		size= imagesize(left, top,right,bottom);
		objeto->msk = new int[size];
		getimage(left, top, right, bottom, objeto->msk);
		cleardevice();	
	}
	
	setactivepage(page);
}


void carregaImagens()
{
	background.image.w = 800;
	background.image.h = 600;
	graphInitObject(&background.image,"imagens/velha.jpg"," ");
	
	escurece.image.w = 800;
	escurece.image.h = 600;
	graphInitObject(&escurece.image," ","imagens/chess_for_ants.jpg");
	
	gota.image.w = 79;
	gota.image.h = 130;
	graphInitObject(&gota.image,"imagens/gota.jpg","imagens/gota_MASK.jpg");
	
	x.image.w = 124;
	x.image.h = 124;
	graphInitObject(&x.image,"imagens/velha_x.jpg","imagens/velha_x_MASK.jpg");

	background.pos.x = 0;
	background.pos.y = 0;
	escurece.pos.x = 0;
	escurece.pos.y = 0;
	
	// frases
	player[0].image.w = 78;
	player[0].image.h = 246;
	graphInitObject(&player[0].image,"imagens/player_verde.jpg","imagens/player_MASK.jpg");
	
	player[1].image.w = 78;
	player[1].image.h = 264;
	graphInitObject(&player[1].image,"imagens/player_vermelho.jpg","imagens/player_MASK.jpg");
	
	cpu[0].image.w = 63;
	cpu[0].image.h = 166;
	graphInitObject(&cpu[0].image,"imagens/cpu_verde.jpg","imagens/cpu_MASK.jpg");
	
	cpu[1].image.w = 63;
	cpu[1].image.h = 166;
	graphInitObject(&cpu[1].image,"imagens/cpu_vermelho.jpg","imagens/cpu_MASK.jpg");
	
	player_venceu.image.w = 562;
	player_venceu.image.h = 46;
	graphInitObject(&player_venceu.image,"imagens/parabains.jpg","imagens/parabains_MASK.jpg");
	
	cpu_venceu.image.w = 302;
	cpu_venceu.image.h = 38;
	graphInitObject(&cpu_venceu.image,"imagens/cpu_venceu.jpg","imagens/cpu_venceu_MASK.jpg");
	
	escolha.image.w = 572;
	escolha.image.h = 47;
	graphInitObject(&escolha.image,"imagens/escolha_gota_canudo.jpg","imagens/escolha_gota_canudo_MASK.jpg");
	
	jogar_novamente.image.w = 442;
	jogar_novamente.image.h = 48;
	graphInitObject(&jogar_novamente.image,"imagens/jogar_novamente.jpg","imagens/jogar_novamente_MASK.jpg");
	
	deu_velha.image.w = 588;
	deu_velha.image.h = 44;
	graphInitObject(&deu_velha.image,"imagens/agua_abaixo.jpg","imagens/agua_abaixo_MASK.jpg");
	
}

bool verPosVencer(char play)
{
	int i, j, cont = 0;

	for(i = 0; i < 3; ++i)
	{
		for(j = 0; j < 3; ++j)
			if(campo[i][j] == play) ++cont;
		
		if(cont == 2)
		{
			for(j = 0; j < 3; ++j)
			{
				if(!campo[i][j])
				{
					campo[i][j] = cpu_play;
					return true;
				}
			}
		}
		
		cont = 0;
	}

	for(j = 0; j < 3; ++j)
	{
		for(i = 0; i < 3; ++i)
			if(campo[i][j] == play) ++cont;
		
		if(cont == 2)
		{
			for(i = 0; i < 3; ++i)
			{
				if(!campo[i][j])
				{
					campo[i][j] = cpu_play;
					return true;
				}
			}
		}

		cont = 0;

	}

	for(i = 0,j = 0; i < 3; ++i,++j)
		if(campo[i][j] == play) ++cont;

	if(cont == 2)
		for(i = 0,j = 0; i < 3; ++i,++j)
			if(!campo[i][j])
			{
				campo[i][j] = cpu_play;
				return true;
			}
		
	cont = 0;

	for(i = 2,j = 0; j < 3; --i,++j)
		if(campo[i][j] == play) ++cont;

	if(cont == 2)
		for(i = 2,j = 0; j < 3; --i,++j)
			if(!campo[i][j])
			{
				campo[i][j] = cpu_play;
				return true;
			}

	return false;
}

void cpuIA()
{
	// 1 - jogada vencedora
	if(verPosVencer(cpu_play)) return;
	// 2 - jogada vencedora do adversario
	else if(verPosVencer(player_play)) return;
	else if(!campo[1][1]) campo[1][1] = cpu_play;
	// 3 - centro
	else if(!campo[0][0]) campo[0][0] = cpu_play;
	// 4 - cantos
	else if(!campo[0][2]) campo[0][2] = cpu_play;
	else if(!campo[2][0]) campo[2][0] = cpu_play;
	else if(!campo[1][0]) campo[1][0] = cpu_play;
	// 5 - laterais
	else if(!campo[2][1]) campo[2][1] = cpu_play;
	else if(!campo[1][2]) campo[1][2] = cpu_play;
	else if(!campo[0][1]) campo[0][1] = cpu_play;
}

bool verificaVitoria(char play)
{
	int i, j, cont = 0;
	
	// exio x
	for(i = 0; i < 3; ++i)
	{
		for(j = 0; j < 3; ++j)
		{
			if(campo[i][j] == play)
			{
				if(++cont == 3)
					return true;
			}
			else
				cont = 0;
		}
		
		cont = 0;
	}

	// eixo y
	for(j = 0; j < 3; ++j)
	{
		for(i = 0; i < 3; ++i)
		{
			if(campo[i][j] == play)
			{
				if(++cont == 3)
					return true;
			}
			else
				cont = 0;
		}
				
		cont = 0;
	}
	
	// diagonal x = y	
	for(i = 0, j = 0; i < 3; ++i,++j)
		if(campo[i][j] == play)
			{
				if(++cont == 3)
					return true;
			}
			else
				cont = 0;
	cont = 0;
	
	// outra diagonal
	for(i = 2, j = 0; j < 3; ++j,--i)
		if(campo[i][j] == play)
			{
				if(++cont == 3)
					return true;
			}
			else
				cont = 0;
				
	
	return false;
}

bool verificaVelha()
{
	int i,j;

	for(i = 0; i < 3; ++i)
		for(j = 0; j < 3; ++j)
			if(!campo[i][j]) return false;
	
	return true;
}
